import Vue from 'vue'
import VueRouter from 'vue-router'

import DefaultLayout from '@/layouts/Default'
import HomePage from '@/pages/Home'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/home',
      components: {
        layout: DefaultLayout,
      },
      children: [
        {
          name: 'Home',
          path: 'home',
          components: {
            page: HomePage,
          },
        },
      ],
    },
  ],
})

export default router
