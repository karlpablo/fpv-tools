const fs = require('fs')
const csvToJson = require('csvtojson')

const dir = '../data/'
const allTests = []

let promiseChain = Promise.resolve()
let fileCount = 0

const ignores = ['.DS_Store']

fs.readdirSync(dir)
  .forEach(file => {
    if (!ignores.includes(file)) {
      fileCount++

      const test = {
        test: file.slice(0, file.length - 4), // remove .csv
        results: []
      }

      console.log(test.test)

      promiseChain = promiseChain.then(async () => {
        await csvToJson()
          .fromFile(`${dir}${file}`)
          .then(data => {
            data.forEach((row, index) => {
              test.results.push([
                parseFloat(row['ESC signal (µs)']),
                parseFloat(row['Thrust (gf)']),
                parseFloat(row['Current (A)']),
                parseFloat(row['Voltage (V)']),
              ])
            })
            allTests.push(test)
          })
          .catch(error => {
            console.log(`${dir}${file}`)
            console.log(error)
          })
      })
    }
  })

promiseChain
  .finally(() => {
    fs.writeFile('./src/data.json', JSON.stringify(allTests), () => {
      console.log(`Processed ${fileCount} CSV files to src/data.json.`)
    })
  })
