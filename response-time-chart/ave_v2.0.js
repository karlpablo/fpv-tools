const parse = require('csv-parse/lib/sync')
const fs = require('fs')
const file = `${process.argv[2]}/RTH.csv`
// const escLow = parseInt(process.argv[3]).toString()
// const escHigh = parseInt(process.argv[4]).toString()

const escLow = '1100'
const escHigh = '1600'

if (!file || !escLow || !escHigh) {
    console.log('Missing parameters!')
    return
}

// logging verbosity, 0 = all, 1 = some, summary only
const verbosity = 2

/* UTILITIES */

// Convert a "second" value to a "millisecond string"
const toMs = (s, dp = 0) => (parseFloat(s) * 1000).toFixed(dp)

// Calculate mean, variance, etc. from an array of numbers
const calculate = (arr) => {
    let mean = arr.reduce((acc, val) => acc + val, 0) / arr.length
    let variance = arr.map(val => Math.pow(Math.abs(val - mean), 2)).reduce((acc, val) => acc + val, 0) / arr.length
    let standardDeviation = Math.sqrt(variance)
    let confidenceInterval95 = 1.960 * (standardDeviation / Math.sqrt(arr.length - 1))

    return {
        mean,
        variance,
        standardDeviation,
        confidenceInterval95,
        length: arr.length,
    }
}

// Get the low and high values of a center point
const range = (val, diff) => {
    return {
        low: val - diff,
        high: val + diff,
    }
}

const sanityCheck = (rpm1, rpm2, rpm3, threshold) => 
    ((rpm2 - rpm1) / rpm1 < threshold && (rpm3 - rpm2) / rpm2 < threshold)

const log = (level, text) => {
    if (level >= verbosity) {
        console.log(text)
    }
}

/* MAIN */

fs.readFile(file, (err, csv) => {
    if (err) throw err

    // parse CSV, remove headers
    let data = parse(csv).slice(1)

    // the corresponding column indexes in the CSV file
    const time = 0
    const esc = 1
    const rpm = 9 // optical

    // sort by time column because most rows are out of order due to high sampling rate
    // according to RCBenchmark, this is normal and the timestamps are still accurate
    data = data.sort((x, y) => x[time] - y[time])

    let rpmIsRising = false,
        rateThreshold = 0.03,
        slopeStart = null,
        results = []

    for (let i = 0; i < data.length - 2; i++) {
        let row = data[i]
        let nextRow = data[i+1]
        let nextRpmRate = (nextRow[rpm] - row[rpm]) / row[rpm]
        let nextTimeDiff = nextRow[time] - row[time]
        let rowLog = `${toMs(row[time]).padStart(5, ' ')}\t${toMs(nextTimeDiff, 2)}\t${row[rpm].padStart(5, ' ')}\t${(nextRpmRate * 100).toFixed(2).padStart(6, ' ')}%  `

        // we only do this on rows with high ESC values
        if (row[esc] === escHigh) {
            if (rpmIsRising) {
                log(0, `${rowLog}\t${toMs(row[time] - slopeStart[time], 2)}`)

                if (nextRpmRate < rateThreshold && sanityCheck(nextRow[rpm], data[i+2][rpm], data[i+3][rpm], rateThreshold)) {
                    results.push({
                        responseTime: row[time] - slopeStart[time],
                        rpmChange: parseInt(row[rpm]) - parseInt(slopeStart[rpm]),
                    })

                    rpmIsRising = false
                }
            } else {
                if (nextRpmRate > rateThreshold) {
                    rpmIsRising = true
                    slopeStart = row
                    log(0, `${rowLog}\t#${results.length+1}`)
                } else {
                    log(0, `${rowLog}`)
                }
            }
        }
    }

    log(1, `=======================`)

    log(1, `ID\tRT\tRPMCh`)
    results.forEach((result, index) => {
        log(1, `${index + 1}\t${toMs(result.responseTime, 2)}\t${result.rpmChange}\t`)
    })

    log(1, `=======================`)

    const allResponseTimes = calculate(results.map(result => result.responseTime))
    const allRpmChanges = calculate(results.map(result => result.rpmChange))

    allResponseTimes.standardDeviationRange = range(allResponseTimes.mean, allResponseTimes.standardDeviation)
    allResponseTimes.confidenceInterval95Range = range(allResponseTimes.mean, allResponseTimes.confidenceInterval95)

    // log(2, `Response Time`)
    // log(2, `  Mean: ${toMs(allResponseTimes.mean, 2)}`)
    // log(2, `  SD: ${toMs(allResponseTimes.standardDeviation, 2)} (${toMs(allResponseTimes.standardDeviationRange.low, 2)} to ${toMs(allResponseTimes.standardDeviationRange.high, 2)})`)
    // log(2, `  95% CI: ${toMs(allResponseTimes.confidenceInterval95, 2)} (${toMs(allResponseTimes.confidenceInterval95Range.low, 2)} to ${toMs(allResponseTimes.confidenceInterval95Range.high, 2)})`)

    // log(2, `RPM`)
    // log(2, `  Mean: ${allRpmChanges.mean.toFixed(0)}`)

    log(2, `${toMs(allResponseTimes.mean, 2)}\t${toMs(allResponseTimes.standardDeviation, 2)}\t${toMs(allResponseTimes.confidenceInterval95, 2)}\t${allRpmChanges.mean.toFixed(0)}`)
})