const parse = require('csv-parse/lib/sync')
const fs = require('fs')
const file = `${process.argv[2]}/THRUST.csv`

/* MAIN */

fs.readFile(file, (err, csv) => {
  if (err) throw err

  // parse CSV, remove headers
  let data = parse(csv).slice(1)

  // the corresponding column indexes in the CSV file
  const esc = 1
  const thrust = 5
  const voltage = 6
  const current = 7
  const rpm = 9 // optical

  let results = []

  for (let i = 0; i < data.length; i++) {
    results.push((parseFloat(data[i][thrust]).toFixed(0)))
  }

  for (let i = 0; i < data.length; i++) {
    results.push((parseFloat(data[i][current]).toFixed(1)))
  }

  for (let i = 0; i < data.length; i++) {
    results.push((parseFloat(data[i][thrust] / (data[i][current] * data[i][voltage])).toFixed(1)))
  }

  console.log(results.join('\t'))
})